import 'reflect-metadata'
import {RouteResultHandler} from './router'
import './di'
import './index.css'


new RouteResultHandler(document.getElementById('root'))
  .start()
