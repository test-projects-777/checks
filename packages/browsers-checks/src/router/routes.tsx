import {NotFoundPage} from '@do-while-for-each/browser-router-react-tools'
import {IEntry} from '@do-while-for-each/browser-router';
import {CanvasPage, ConstantDistance, CrispLine, EventLoopPage, ImageToFit, IndexPage, IsPointIn, ResizeObserver, TilesMaker, ToNewCoordinateSystem, TransformsPage} from '../app/page'

export const routes: IEntry[] = [
  {segment: '', component: <IndexPage/>},
  {segment: 'event-loop', component: <EventLoopPage/>},
  {
    segment: 'transforms', component: <TransformsPage/>, children: [
      {segment: 'constant-distance', component: <ConstantDistance/>},
      {segment: 'resize-observer', component: <ResizeObserver/>},
      {segment: 'to-new-coordinate-system', component: <ToNewCoordinateSystem/>},
    ]
  },
  {
    segment: 'canvas', component: <CanvasPage/>, children: [
      {segment: 'image-to-fit', component: <ImageToFit/>},
      {segment: 'crisp-line', component: <CrispLine/>},
      {segment: 'is-point-in', component: <IsPointIn/>},
      {segment: 'tiles-maker', component: <TilesMaker/>},
    ]
  },
  {segment: 'not-found', component: <NotFoundPage/>},
  {segment: '**', redirectTo: '/not-found'},
]
