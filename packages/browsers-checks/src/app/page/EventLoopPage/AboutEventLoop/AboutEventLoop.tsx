import {Microtasks} from './Microtasks';

export const AboutEventLoop = () => {
  return (
    <div>
      <h1>About the EventLoop</h1>
      <Microtasks/>
    </div>
  );
}
