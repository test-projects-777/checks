import {useSubjState} from '@do-while-for-each/rxjs-react'
import {useEffect} from 'react'
import {TimerInput} from '../TimerInput/TimerInput'
import {TimerFactory} from '../TimerFactory'

export function Timer() {
  const [count, setCount] = useSubjState('1')
  const [interval, setInterval] = useSubjState('0.3')

  useEffect(() =>
      TimerFactory.forEffect(count.value$, interval.value$)
    , [count, interval])

  return (
    <div>
      <h3>Timers</h3>
      <TimerInput label="count" value={count.lastValue} setValue={setCount}/>&nbsp;&nbsp;&nbsp;
      <TimerInput label="interval" value={interval.lastValue} setValue={setInterval}/>
    </div>
  )
}
